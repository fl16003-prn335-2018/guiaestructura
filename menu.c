#include <stdio.h>

int main()
{
    char opcion;
    int n1, n2, r;
    int *p1;
    int *p2;
    int *p3;

    do
    {
        printf( "\n   >>> MENU CALCULADORA <<<" );
        printf( "\n\n   1. Sumar dos numeros.");
        printf( "\n   2. Restar dos numeros." );
        printf( "\n   3. Ver direcciones en memoria de las variables." );
        printf( "\n   4. Salir.\n" );

        
        do
        {
            printf( "\n   Introduzca opcion (1-4): ");
            fflush( stdin );
            scanf( "%c", &opcion);

        } while ( opcion < '1' || opcion > '4' );
        

        switch ( opcion )
        {
                     
            case '1': printf( "\n   Ingrese el primer numero: " );
                      scanf( "%d", &n1);
                      p1=&n1;
                      printf( "\n   Ingrese el segundo numero: " );
                      scanf( "%d", &n2);
                      p2=&n2;
                      r=n1+n2;
                      p3=&r;
                      printf( "\n Resultado de la suma: %d\n", r );
                      printf( "\n *p1 + p*2=%d\n", *p1+*p2);
                      
                      
                      break;

                      
            case '2': printf( "\n   Ingrese el primer numero: " );
                      p1=&n1;
                      scanf( "%d", &n1);
                      p2=&n2;
                      printf( "\n   Ingrese el segundo numero: " );
                      scanf( "%d", &n2);
                      r=n1-n2;
                      p3=&r;
                      printf( "\n Resultado de la resta: %d\n", r );
                      printf( "\n *p1 - p*2=%d\n", *p1-*p2);
                      
                      break;
                      
                      
          case '3' :  p1=&n1;
                      p2=&n2;
                      p3=&r;
                      printf("\n La direccion de n1 es %p\n La direccion de n2 es %p\n La direccion de r es %p\n\n", &p1, &p2, &p3);       


        }

    } while ( opcion != '4' );

    return 0;
}



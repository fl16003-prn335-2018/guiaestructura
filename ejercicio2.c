
#include <stdio.h>
#include <stdlib.h>
int main() {
	int i;
	int vector1[5]={1,2,3,4,5};
	int vector2[5]={6,7,8,9,10};
	int *puntero1=&vector1[5];
	int *puntero2=&vector2[5];
	int aux;
	
	printf("Vector 1 ANTES\n");
	for (i=5;i>0;i--){
		printf("%d\n",*(puntero1-i));
	}
	printf("Vector 2 ANTES\n");
	for (i=5;i>0;i--){
		printf("%d\n",*(puntero2-i));
	}
	for (i=5;i>0;i--){
		aux=*(puntero2-i);
		*(puntero2-i)=*(puntero1-i);
		*(puntero1-i)=aux;
	}
	printf("Vector 1 DESPUES\n");
	for (i=5;i>0;i--){
		printf("%d\n",*(puntero1-i));
	}
	printf("Vector 2 DESPUES\n");
	for (i=5;i>0;i--){
		printf("%d\n",*(puntero2-i));
	}
	return 0;
}
